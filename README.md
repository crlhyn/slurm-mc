# Creating iCLEA

iCLEA templates : Cloud Easy Appliances ready to deploy & build Clouds, time & cost effectively as in the  home furniture model. 


 DEMO template includes :
 -  slurm montecarlo automation with scripts and ansible
 - tf 



[TOC]


,
   
 

 [[_TOC_]]



## Slurm MC basic scripts to simplify SLURM installations


To avoid the old guides procedures using old systems setups or re-packaging or re-compilations ...

Let us do a standard system installation with  lighter and simpler steps :


## munge

install

```bash
 apt install iproute2   -y
 export MUNGEUSER=3100
 groupadd -g $MUNGEUSER munge
 useradd -m -c "MUNGE Uid 'N' Gid" -d /var/lib/munge -u $MUNGEUSER -g munge -s /sbin/nologin munge
 apt  install munge -y 
 #apt-get install libmunge-dev
service munge start
 * Starting MUNGE munged
```

verify:

```
 munge -n
MUNGE:AwQFAAC4TPQe8XVEQG7jqDKn8Rne+pZo8XUIJbLUzERGceqcheEJz46Xp6jIfd1Tso6nR00OkqvVR8HJIkS7NVI5m9D8eO+vYNGZN3MeZRySGcjO2PVf3QY973RssGXrw17Umjw=:
$ munge -n | ssh 172.17.0.7 unmunge
STATUS:           Success (0)
ENCODE_HOST:      ??? (172.17.0.8)
ENCODE_TIME:      2021-04-16 12:56:16 +0200 (1618570576)
DECODE_TIME:      2021-04-16 12:56:31 +0200 (1618570591)
TTL:              300
CIPHER:           aes128 (4)
MAC:              sha256 (5)
ZIP:              none (0)
UID:              ansible (1000)
GID:              ansible (1000)
LENGTH:           0
```

## slurm

review config:
```
CPU=24 Sockets=2 ...
```
or use on cfg then run
       export SLURM_CONF=/path/to/your/copy

i.e. instead of compiling last https://download.schedmd.com/slurm/slurm-20.11.3.tar.bz2 to install ubuntu packaged v. 17 or 18 

### Simple installations

Some  scripts and notes are pakaged in ansible   for documentation

One of the advantages/ goals of ansible is being declarative (define how the system should be). In declarative, There will be no changes once the system arrives to its declared goal. 

Instead, we use ansible shell tasks for human consistency: these are procedural tasks hence it serve to a) document for those wishing to run the command by command, and b) yet let us automate send commands in batch to many sites simultaneusly, 



### Usage examples

command | meaning
---|---
sbatch |	submit a batch job script for queueing and execution
salloc/xalloc |	submit an interactive job request
srun 	| run a command within an existing job, on a subset of allocated resources
scancel |	cancel a queued or running job
squeue |	query the status of your job(s) or the job queue



## Best Practices
- specify only what's necessary, leave open  flexible parameters
- specify complete, accurate, and flexible resource requirements


### bibliography
- []  https://www.nccs.nasa.gov/nccs-users/instructional/using-slurm
- https://slurm.schedmd.com/quickstart.html  ![arch pic](https://slurm.schedmd.com/arch.gif)

### Other Notes

for future container packaging 

#### Pull with Singularity

     singularity pull --arch amd64 library://crlhyn/remote-builds/rb-6027a83271bec9ed56679233:latest

#### Pull by unique ID (reproducible even if tags change)
     singularity pull library://crlhyn/remote-builds/rb-6027a83271bec9ed56679233:sha256.4215b9849c62eac8d85f1abcc064f964c27cdd3150b3609bec0d6012df5578fa

- Hub, Stanford U. https://singularity-hub.org/ and gitHub
- Cloud,  https://cloud.sylabs.io/ and GitLab

Config
```bash
Bootstrap:docker  
From:ubuntu:18.04

%labels
MAINTAINER crlhyn
WHATAMI ubu18

%environment
Hi_WORLD="DATA to DISCOVER"
export Hi_WORLD

%files
#hi.sh /hi.sh

%runscript
exec /bin/bash echo /hi.sh "$@"

%test
echo DISCOVERY /hi.sh | grep DISCOVER

%post
echo chmod u+x /hi.sh
```
