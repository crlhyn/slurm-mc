#!/usr/bin/csh
#SBATCH -J Test_Job
#SBATCH --nodes=4 --ntasks=24 --cpus-per-task=2 --ntasks-per-node=6
#SBATCH --constraint=hasw
#SBATCH --time=12:00:00
#SBATCH -o output.%j
#SBATCH --account=xxxx
source /usr/share/modules/init/csh
module purge
module load comp/intel-13.1.3.192 mpi/impi-4.1.0.024
cd $SLURM_SUBMIT_DIR
setenv OMP_NUM_THREADS 2
setenv OMP_STACKSIZE 1G
setenv KMP_AFFINITY compact
setenv I_MPI_PIN_DOMAIN auto
mpirun -perhost 6 -np 24 ./Test_executable
