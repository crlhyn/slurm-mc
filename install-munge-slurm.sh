 sudo apt install munge

#Test 

  munge -n | unmunge

  sudo apt install mariadb-server
$ sudo mysql -u root
create database slurm_acct_db;
create user 'slurm'@'localhost';
set password for 'slurm'@'localhost' = password('slurmdbpass');
grant usage on *.* to 'slurm'@'localhost';
grant all privileges on slurm_acct_db.* to 'slurm'@'localhost';
flush privileges;
exit

sudo apt install slurmd slurm-client slurmctld

dpkg -l | grep slurm

curl https://www.schedmd.com/archives.php 
cd slurm-17.11.10
$ ./configure
$ make html
$ xdg-open doc/html/configurator.html

setup per 
https://blog.llandsmeer.com/tech/2020/03/02/slurm-single-instance.html

    Copy-paste to /etc/slurm-llnll/slurm.conf.

Create a file /etc/slurm-llnl/cgroup.conf:

CgroupAutomount=yes
CgroupReleaseAgentDir="/etc/slurm/cgroup"
ConstrainCores=yes
ConstrainDevices=yes
ConstrainRAMSpace=yes

#Restart daemons
sudo systemctl restart slurmctld
sudo systemctl restart slurmd

#Running sinfo should show no errors:
 sinfo
#PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
#debug*       up   infinite      1   idle a715




#######3

    useradd slurm 
    mkdir -p /etc/slurm /etc/slurm/prolog.d /etc/slurm/epilog.d /var/spool/slurm/ctld /var/spool/slurm/d /var/log/slurm
    chown slurm /var/spool/slurm/ctld /var/spool/slurm/d /var/log/slurm

# can do to ansible nodes: Copy into place config files from this repo which you've already cloned into /storage
    cp /storage/ubuntu-slurm/slurmdbd.service /etc/systemd/system/
    cp /storage/ubuntu-slurm/slurmctld.service /etc/systemd/system/
