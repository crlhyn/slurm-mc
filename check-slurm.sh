

echo Run sleep 1 on 8 processors:

 time srun -n8 sleep 1
#srun -n8 sleep 1  -- 1,20s (0,01s(u) + 0,00s(s) 6kb 0+49 ctx)

#debug
slurmctld -D
  slurmd -D
  sinfo



#setup by node 
for node in $(sinfo -o "%n" -h|grep -v `hostname`)
do
  ssh $node 'wget https://gist.github.com/YidingZhou/2bd806b228a25d3d84da/raw/slurmdemo.py'
  ssh $node 'wget https://raw.githubusercontent.com/Azure/azure-batch-samples/master/Python/Storage/blobxfer.py'
  ssh $node 'sudo apt-get install -y imagemagick'
done


# to the slurm headnode:

ssh -i <path/to/private/key.pem> centos@<public-ip-address>

sbatch test.sbatch
